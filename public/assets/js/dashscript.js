// Get menu items and destructuring to array
let sideList = [...document.querySelectorAll('.db-sidebar-nav__link')]
let sideListSub = [...document.querySelectorAll('.db-sidebar-subnav__link')]
// "Selected" classes
let selectedClass = `db-sidebar-nav__link--selected`
let selectedClassSub = `db-sidebar-subnav__link--selected`

// Menu dropdown and select - first check submenus and add classes
sideList.forEach(element => {
    if (element.parentElement.children.length > 1) element.classList.add('submenu-show')
    // Click event to clean all other selecteds and add new one to target
    element.addEventListener('click', () => {
        let current = document.querySelector(`.${selectedClass}`);
        let currentSub = document.querySelector(`.${selectedClassSub}`)

        if (current && current !== element) {
            current.classList.remove(selectedClass)
            current.classList.remove('submenu-show--open')
        }
        if (currentSub) currentSub.classList.remove(selectedClassSub)

        if (element.parentElement.children.length > 1) {
            element.classList.toggle(selectedClass)
            element.classList.toggle('submenu-show--open')
        } else {
            element.classList.add(selectedClass)
        }
    })
})

// Submenu select
sideListSub.forEach(element => {
    // Click event to check if the class exists, clean, and add new one to target
    element.addEventListener('click', () => {
        let currentSub = document.querySelector(`.${selectedClassSub}`)

        if (currentSub) currentSub.classList.remove(selectedClassSub)

        element.classList.add(selectedClassSub)
    })
})

//On smartphones show side bar when click menu and hide when click out
let dbSideMenu = document.querySelector('.db-sidebar')
let curtain = document.querySelector('.curtain')

dbSideMenu.addEventListener('click', () => {
    if (window.innerWidth <= 600) {
        dbSideMenu.classList.add('db-sidebar-open')
        curtain.classList.remove('curtain--off')
    }
})
document.querySelector('.db-main').addEventListener('click', () => {
    dbSideMenu.classList.remove('db-sidebar-open')
    curtain.classList.add('curtain--off')
})
document.querySelector('.db-nav').addEventListener('click', () => {
    dbSideMenu.classList.remove('db-sidebar-open')
    curtain.classList.add('curtain--off')
})

//Render date correct
// document.addEventListener('DOMContentLoaded', function () {
//     let listItems = [...document.querySelectorAll('.list-articles__item')];
//     listItems.map(date => date.children[0].innerHTML = renderDate(date.children[0].innerHTML))

//     function renderDate(dateModified) {
//         var data = new Date(dateModified);
//         var dia = data.getDate();
//         if (dia.toString().length == 1)
//             dia = "0" + dia;
//         var mes = data.getMonth() + 1;
//         if (mes.toString().length == 1)
//             mes = "0" + mes;
//         var ano = data.getFullYear();
//         return dia + "-" + mes + "-" + ano;
//     }
// }, false);

//Sort list by Date
document.querySelector('.list-header__col-title-date').addEventListener('click', () => {
    let listItems = [...document.querySelectorAll('.list-articles__item')];
    let list = document.querySelector('.list-articles');
    let title = document.querySelector('.list-header__col-title-article')
    let dateTitle = document.querySelector('.list-header__col-title-date')

    const sortDateAsc = arr => {
        const toDate = dateStr => {
            let parts = dateStr.split("-")
            return new Date(parts[2], parts[1] - 1, parts[0])
        }
        return arr.sort((a, b) => new Date(toDate(a.children[0].innerHTML)) - new Date(toDate(b.children[0].innerHTML)))
    }

    const sortDateDesc = arr => {
        const toDate = dateStr => {
            let parts = dateStr.split("-")
            return new Date(parts[2], parts[1] - 1, parts[0])
        }
        return arr.sort((a, b) => new Date(toDate(b.children[0].innerHTML)) - new Date(toDate(a.children[0].innerHTML)))
    }

    title.classList.remove('list-header__col-title-article--asc')
    title.classList.remove('list-header__col-title-article--desc')

    if (dateTitle.classList.contains('list-header__col-title-date--desc')) {
        sortDateAsc(listItems).map(a => list.insertAdjacentElement('beforeend', a))
        dateTitle.classList.remove('list-header__col-title-date--desc')
        dateTitle.classList.add('list-header__col-title-date--asc')
    } else if (dateTitle.classList.contains('list-header__col-title-date--asc')) {
        sortDateDesc(listItems).map(a => list.insertAdjacentElement('beforeend', a))
        dateTitle.classList.remove('list-header__col-title-date--asc')
        dateTitle.classList.add('list-header__col-title-date--desc')
    } else {
        sortDateAsc(listItems).map(a => list.insertAdjacentElement('beforeend', a))
        dateTitle.classList.add('list-header__col-title-date--asc')
    }
})

//Sort list by Name
document.querySelector('.list-header__col-title-article').addEventListener('click', () => {
    let listItems = [...document.querySelectorAll('.list-articles__item')];
    let list = document.querySelector('.list-articles');
    let title = document.querySelector('.list-header__col-title-article')
    let dateTitle = document.querySelector('.list-header__col-title-date')

    const sortTitleAsc = arr => {
        return arr.sort((a, b) => a.children[1].innerHTML.toLowerCase().localeCompare(b.children[1].innerHTML.toLowerCase()))
    }

    const sortTitleDesc = arr => {
        return arr.sort((a, b) => b.children[1].innerHTML.toLowerCase().localeCompare(a.children[1].innerHTML.toLowerCase()))
    }

    dateTitle.classList.remove('list-header__col-title-date--asc')
    dateTitle.classList.remove('list-header__col-title-date--desc')

    if (title.classList.contains('list-header__col-title-article--desc')) {
        sortTitleAsc(listItems).map(a => list.insertAdjacentElement('beforeend', a))
        title.classList.remove('list-header__col-title-article--desc')
        title.classList.add('list-header__col-title-article--asc')
    } else if (title.classList.contains('list-header__col-title-article--asc')) {
        sortTitleDesc(listItems).map(a => list.insertAdjacentElement('beforeend', a))
        title.classList.remove('list-header__col-title-article--asc')
        title.classList.add('list-header__col-title-article--desc')
    } else {
        sortTitleAsc(listItems).map(a => list.insertAdjacentElement('beforeend', a))
        title.classList.add('list-header__col-title-article--asc')
    }
})